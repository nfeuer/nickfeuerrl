﻿using UnityEngine;
using System.Collections;

public class ActiveSpace : MonoBehaviour {
    GameObject spot;
    SpriteRenderer rend;
    Color dorm = new Color(255, 255, 255, 0.2f);
    Color activ = new Color(255, 255, 255, 0.4f);

	// Use this for initialization
	void Start () {
        spot = gameObject;
        rend = spot.GetComponent<SpriteRenderer>();
	}
	
	void OnMouseEnter()
    {
        iTween.ValueTo(spot, iTween.Hash("from", rend.color, "to", activ, "time", 0.3f, "onupdate", "Visibility"));
    }

    void OnMouseExit()
    {
        iTween.ValueTo(spot, iTween.Hash("from", rend.color, "to", dorm, "time", 0.3f, "onupdate", "Visibility"));
    }

    void Visibility(Color color)
    {
        rend.color = color;
    }
}
