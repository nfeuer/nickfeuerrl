﻿using UnityEngine;
using System.Collections;

public class BoardGenerator2 : MonoBehaviour {

	public GameObject canvas;
	public GameObject boardPanel;
    public static Sprite[] allCards;
    public float width = 10;
    public static float height = 10;
    float gridW;
    public static float gridH;
    public static float cardW;
    public static float cardH;
    public string[] topBoard;
    string testing = "customInside"; //Only for use with my custom file
    public string[] middle; //Only for use with my custom file
	public Camera camera;
    // Use this for initialization
    void Start()
    {
        allCards = Resources.LoadAll<Sprite>("Sprite/CardDeck21");
        //======================================================
        //              Board Generator Script
        //======================================================

        RectTransform parentRect = null;
		if (canvas != null) {
			parentRect = canvas.GetComponent<RectTransform> ();
		} 
		

		RectTransform boardPanelRect = boardPanel.GetComponent<RectTransform> ();
		width = boardPanelRect.rect.width;
		Debug.Log ("width: " + width);
		height = boardPanelRect.rect.height;
		Debug.Log ("height: " + height);

		TextAsset textAsset = Resources.Load<TextAsset>("Text/"+AppModel.levelChoice);
		Debug.Log(AppModel.levelChoice);
        string data = textAsset.text;
        Debug.Log(data);
        topBoard = data.Split("\n"[0]);
//        middle = new string[] { "a", "a", "a", "a"}; //Only for use with my custom file
//        if(testing.Equals(textAsset.name)) { //Only for use with my custom file
//            Debug.Log(topBoard.Length);
//            for (int i = 0; i < 4; i++)
//            {
//                middle[i] = topBoard[i];
//            }
//            middle[3] = "2";
//            topBoard = middle;
//            Debug.Log(topBoard.Length);
//        }

		int horCells = topBoard [0].Split (',').Length;
		int verCells = ((2*topBoard.Length) - 1);

		gridW = width / horCells;
        Debug.Log(gridW);
		gridH = height / verCells;
        Debug.Log(gridH);
        cardH = gridH * 0.8f;
        cardW = cardH * 0.75f;

		//testing

		float canvasHeight = 600;
		float panelXOffset = 0;
		float panelYOffset = 0;
		if (canvas != null) {
			canvasHeight = parentRect.rect.height;
			panelXOffset = boardPanelRect.anchoredPosition3D.x;
			panelYOffset = boardPanelRect.anchoredPosition3D.y;

		}

		Debug.Log("x: "+boardPanelRect.anchoredPosition3D.x+" y: "+boardPanelRect.anchoredPosition3D.y); 
		//

		Vector3 init = new Vector3(panelXOffset, canvasHeight+panelYOffset,0); //Uniform starting position of each object created
        Vector3 horMov = new Vector3(gridW, 0, 0); //Horizontal increment
        Vector3 verMov = new Vector3(0, gridH, 0); //Vertical increment
        //Vector3 padding = new Vector3(padW, padH, 0);

        for (int i = 0; i < topBoard.Length - 1; i++)
        {
            int prog = -topBoard.Length - (i - topBoard.Length);
            Debug.Log(i);
            string[] row = topBoard[i].Split(","[0]);

			GameObject spotRow = new GameObject ();
			spotRow.name = "Enemy Row" + i;
			spotRow.transform.SetParent (boardPanel.transform);

            for (int j = 0; j < row.Length; j++)
            {
                int spotValue;
                int.TryParse(row[j], out spotValue);

                GameObject spot;
                SpriteRenderer rend;
                spot = new GameObject();
                spot.name = "r" + (-prog) + "c" + j;
                spot.transform.position = init + -i * verMov + j * horMov;
				spot.transform.SetParent (spotRow.transform);
                //spot.transform.position = init + prog * verMov + j * horMov;
                spot.transform.localScale = new Vector3(gridW, gridH, 1);
                rend = spot.AddComponent<SpriteRenderer>();
                rend.sprite = Resources.Load("Sprite/Square", typeof(Sprite)) as Sprite;
                switch (spotValue)
                {
                    case 0:
                        rend.color = new Color(255, 255, 255, 0.01f);
                        //spot.transform.localScale = new Vector3(cardW, cardH, 1);
                        //No script, therefore not active
                        break;
					case 1:
						rend.color = new Color (255, 255, 255, 0.2f);
                        //spot.transform.localScale = new Vector3(cardW - (2 * padW), cardH - padH, 1);
						spot.AddComponent<BoxCollider2D> ();
						spot.AddComponent<ActiveSpace> ();
						spot.AddComponent<CanvasRenderer> ();
                        break;
                }
            }
        }

		int channelNum;
		int.TryParse (topBoard [topBoard.Length - 1], out channelNum);
		Vector3 center = new Vector3(width/2 + panelXOffset, canvasHeight+panelYOffset-(boardPanelRect.rect.height/2), 0);

		GameObject channelRow = new GameObject ();
		channelRow.name = "Channel";
		channelRow.transform.SetParent (boardPanel.transform);

		switch (channelNum) {
		case 1:
			Vector3 v = new Vector3 (-gridW / 2, gridH/2, 0);
			makeBox ("c1", center + v, channelRow);
			break;
		case 2:
			Vector3 v1 = new Vector3 (-gridW, gridH/2, 0);
			Vector3 v2 = new Vector3 (0, gridH/2, 0);
			makeBox ("c1", center + v1, channelRow);
			makeBox ("c2", center + v2, channelRow);
			break;
		}


        for (int i = topBoard.Length - 2; i >= 0; i--) //Reverse direction for bottom half of board
        {
            int prog = topBoard.Length - (i - (topBoard.Length - 2)) - 1; //Continue counting correct row
            string[] row = topBoard[i].Split(","[0]);

			GameObject spotRow = new GameObject ();
			spotRow.name = "Friendly Row" + i;
			spotRow.transform.SetParent (boardPanel.transform);

            for (int j = 0; j < row.Length; j++)
            {
                int spotValue;
                int.TryParse(row[j], out spotValue);

                GameObject spot;
                SpriteRenderer rend;
                spot = new GameObject(); //Create object at spot
                spot.name = "r" + prog + "c" + j;
                //spot.transform.position = init + prog * verMov * -1 + j * horMov + padding;
				spot.transform.SetParent (spotRow.transform);
				spot.transform.position = init + i * verMov + j * horMov - (verCells-1)*verMov;
                spot.transform.localScale = new Vector3(gridW, gridH, 1);
                rend = spot.AddComponent<SpriteRenderer>();
                rend.sprite = Resources.Load("Sprite/Square", typeof(Sprite)) as Sprite;
                switch (spotValue) //Determine if spot is interactable
                {
                    case 0:
                        rend.color = new Color(255, 255, 255, 0.01f);
                        //spot.transform.localScale = new Vector3(cardW, cardH, 1);
                        //No script, therefore not active
                        break;
                    case 1:
                        rend.color = new Color(255, 255, 255, 0.2f);
                        //spot.transform.localScale = new Vector3(cardW - (2 * padW), cardH - padH, 1);
                        spot.AddComponent<BoxCollider2D>();
                        spot.AddComponent<ActiveSpace>();
                        break;
                }
            }
        }

        //==================================================================
        //                              Cards
        //==================================================================
        for(int i = -1; i < 2; i++) //Top row cards
        {
            GameObject nCard;
            GameObject cardbord;
            nCard = new GameObject();
            //nCard.transform.position = new Vector3(width / 2 + (i*(width/4)), height / 4 * 3, -1);
            nCard.transform.position = new Vector3(-width / 6, height / 6 * 5 + (i*cardH), -1);
            cardbord = new GameObject();
            //cardbord.transform.position = new Vector3(width / 2 + (i * (width / 4)), height / 4 * 3, -1);
            cardbord.transform.position = new Vector3(-width / 6, height / 6 * 5 + (i * cardH), -1);
            nCard.name = "Card " + i;
            cardbord.name = nCard.name + " Border";
            cardbord.transform.SetParent(nCard.transform);
            nCard.AddComponent<MouseClick2>();
        }

        for (int i = -1; i < 2; i++) //Bottom row cards
        {
            GameObject nCard;
            GameObject cardbord;
            nCard = new GameObject();
            nCard.transform.position = new Vector3(width / 2 + (i * (width / 4)), height / 4, -1); //For staged field
            //nCard.transform.position = new Vector3(-width / 6, height / 6 + (i * cardH), -1);
            cardbord = new GameObject();
            cardbord.transform.position = new Vector3(width / 2 + (i * (width / 4)), height / 4, -1); // For staged field
            //cardbord.transform.position = new Vector3(-width / 6, height / 6 + (i * cardH), -1);
            nCard.name = "Card " + i;
            cardbord.name = nCard.name + " Border";
            cardbord.transform.SetParent(nCard.transform);
            nCard.AddComponent<MouseClick2>();
        }


    }

    void makeBox(string name, Vector3 v, GameObject parent){
		GameObject spot;
		SpriteRenderer rend;
		spot = new GameObject();
		spot.name = name;
		spot.transform.position = v;
		spot.transform.SetParent (parent.transform);
		//spot.transform.position = init + prog * verMov + j * horMov;
		spot.transform.localScale = new Vector3(gridW, gridH, 1);
		rend = spot.AddComponent<SpriteRenderer>();
		rend.sprite = Resources.Load("Sprite/Square", typeof(Sprite)) as Sprite;
		rend.color = new Color(255, 255, 255, 0.2f);
		spot.AddComponent<BoxCollider2D>();
		spot.AddComponent<ActiveSpace>();
	}

    // Update is called once per frame
    void Update()
    {

    }
}
