﻿using UnityEngine;
using System.Collections;

public class BordGenerator : MonoBehaviour {
    
    int width = 337;
    int height = 600;
    float cardW;
    float cardH;
    string[] topBoard;
	public GameObject canvas;
    

	// Use this for initialization
	void Start () {
        //bord = new GameObject();
        //rendered = bord.AddComponent<SpriteRenderer>();
        //bord.name = card.name + " Bord";
        //rendered.sprite = Resources.Load("Sprite/Square", typeof(Sprite)) as Sprite;
        //rendered = bord.GetComponent<SpriteRenderer>();
        //rendered.color = new Color(0, 199, 238, 0);
        //bord.transform.localScale = new Vector3(0.7644346f, 1.021202f, 1);
        //bord.transform.position = card.transform.position + adjust;
        //toColorT = new Color(0, 199, 238, 1);
        //fromColorT = rendered.color;
        //toColorF = new Color(255, 0, 0, 1);
        //fromColorF = new Color(255, 0, 0, 0);

        //======================================================
        //              Board Generator Script
        //======================================================
        TextAsset textAsset = Resources.Load<TextAsset>("hugeoutside");
        string data = textAsset.text;
        topBoard = data.Split("\n"[0]);
		int horCellNum = topBoard [0].Split (',').Length;
		int verCellNum = (topBoard.Length * 2) - 1;

		cardW = width / horCellNum;
		cardH = height / verCellNum;
        Vector3 init = new Vector3(0, cardH, 0);
        Vector3 horMov = new Vector3(cardW, 0, 0); //Horizontal increment
        Vector3 verMov = new Vector3(0, cardH, 0); //Vertical increment

        for (int i = 0; i < topBoard.Length - 1; i++)
        {
			GameObject spotRow = new GameObject ();
			spotRow.name = "Friendly Row" + (topBoard.Length - i - 2);
			spotRow.transform.SetParent (canvas.transform);

            string[] row = topBoard[i].Split(","[0]);
            for(int j = 0; j < row.Length; j++)
            {
                int spotValue;
                int.TryParse(row[j], out spotValue);

                GameObject spot;
                SpriteRenderer rend;
                spot = new GameObject();
				spot.transform.SetParent (spotRow.transform);
                spot.name = "r" + i + "c" + j;
                spot.transform.position = init + i * verMov + j * horMov;
                spot.transform.localScale = new Vector3(cardW, cardH, 1);
                rend = spot.AddComponent<SpriteRenderer>();
                rend.sprite = Resources.Load("Sprite/Square", typeof(Sprite)) as Sprite;
                switch (spotValue)
                {
                    case 0:                        
                        rend.color = new Color(0, 0, 0, 0.2f);
                        //No script, therefore not active
                        break;
                    case 1:                       
                        rend.color = new Color(0, 0, 0, 0.8f);
                        //Attach active script
                        break;
                }
            }
        }

		GameObject channelRow = new GameObject ();
		channelRow.transform.SetParent (canvas.transform);
		channelRow.name = "Row C";
		int channelSpotNum;
		int.TryParse (topBoard [topBoard.Length - 1], out channelSpotNum);
		switch (channelSpotNum) {
		case 1:
			renderBox ("c1", init + topBoard.Length * verMov + (new Vector3 ((width / 2) - (cardW / 2), 0, 0)), channelRow);
			break;
		case 2:
			renderBox ("c1", init + topBoard.Length * verMov + (new Vector3 ((width / 2) - (cardW), 0, 0)), channelRow);
			renderBox ("c2", init + topBoard.Length * verMov + (new Vector3 ((width / 2) + (cardW), 0, 0)), channelRow);
			break;
		} 


        for (int i = topBoard.Length - 2; i >= 0; i--) //Reverse direction for bottom half of board
        {
            int prog = topBoard.Length - (i - topBoard.Length + 2); //Continue counting correct row

			GameObject spotRow = new GameObject ();
			spotRow.name = "Enemy Row" + (topBoard.Length - i - 2);
			spotRow.transform.SetParent (canvas.transform);

            string[] row = topBoard[i].Split(","[0]);
            for (int j = 0; j < row.Length; j++)
            {
                int spotValue;
                int.TryParse(row[j], out spotValue);

                GameObject spot;
                SpriteRenderer rend;
                spot = new GameObject(); //Create object at spot
                spot.name = "r" + prog + "c" + j;
				spot.transform.SetParent (spotRow.transform);
                spot.transform.position = init + prog * verMov + j * horMov;
                spot.transform.localScale = new Vector3(cardW, cardH, 1);
                rend = spot.AddComponent<SpriteRenderer>();
                rend.sprite = Resources.Load("Sprite/Square", typeof(Sprite)) as Sprite;
                switch (spotValue) //Determine if spot is interactable
                {
                    case 0:
                        rend.color = new Color(0, 0, 0, 0.2f);
                        //No script, therefore not active
                        break;
                    case 1:
                        rend.color = new Color(0, 0, 0, 0.8f);
                        //Attach active script
                        break;
                }
            }
        }

    }

	void renderBox(string name, Vector3 v, GameObject spotRow){
		GameObject spot;
		SpriteRenderer rend;
		spot = new GameObject();
		spot.transform.SetParent (spotRow.transform);
		spot.name = name;
		spot.transform.position = v;
		spot.transform.localScale = new Vector3(cardW, cardH, 1);
		rend = spot.AddComponent<SpriteRenderer>();
		rend.sprite = Resources.Load("Sprite/Square", typeof(Sprite)) as Sprite;
	}

	// Update is called once per frame
	void Update () {
	
	}
}
