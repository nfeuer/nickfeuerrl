﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class Bounce : MonoBehaviour {

    public float px = 580;
    public float py = 270;
    public float xvelocity = -100;
    public float yvelocity = -100;
    public bool completex = true;
    public bool completey = true;
    public int counterx = 0;
    public int countery = 0;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() { 

        px += xvelocity * Time.deltaTime;
        py += yvelocity * Time.deltaTime;
        iTween.MoveUpdate(gameObject, iTween.Hash("x", px, "y", py));
        Wall();
	    
	}

    void Uncomp(string ax)
    {
        
        if(ax.Equals("x"))
        {            
            xvelocity = -xvelocity;
            iTween.ScaleTo(gameObject, iTween.Hash("name", "uncompx", "scale", new Vector3(5, 5, 0), "time", 0.24, "easetype", "linear", "oncomplete", "StopIt", "oncompletetarget", this.gameObject, "oncompleteparams", "uncompx"));
            //iTween.ColorTo(gameObject, iTween.Hash("name", "fadeback", "color", new Color(137F,150F,196F), "a", 255, "easetype", "linear", "time", 0.18));
        }
        if (ax.Equals("y"))
        {
            yvelocity = -yvelocity;
            iTween.ScaleTo(gameObject, iTween.Hash("name", "uncompy", "scale", new Vector3(5, 5, 0), "time", 0.24, "easetype", "linear", "onComplete", "StopIt", "oncompletetarget", this.gameObject, "oncompleteparams", "uncompy"));
            //iTween.ColorTo(gameObject, iTween.Hash("name", "fadeback", "color", new Color(137F, 150F, 196F), "r", 137, "g", 150, "b", 196, "a", 255, "easetype", "linear", "time", 0.18));
        }
        StopIt("comp" + ax);
        
    }

    void StopIt(string nam)
    {
        iTween.StopByName(nam);
        if (nam.Equals("uncompx"))
        {
            completex = true;
            //EditorUtility.DisplayDialog("Bottom", "Stopped " + nam, "Ok", "");
        }
        if (nam.Equals("uncompy"))
        {
            completey = true;
            //EditorUtility.DisplayDialog("Bottom", "Stopped " + nam, "Ok", "");
        }

    }

    void Wall()
    {
        if(px <= 16 && completex || px >= 1057 && completex)
        {
            completex = false;
            iTween.ScaleTo(gameObject, iTween.Hash("name", "compx", "scale", new Vector3(3,5,0), "time", 0.2, "easetype", "linear", "onComplete", "Uncomp", "oncompletetarget", this.gameObject, "oncompleteparams", "x"));
            iTween.FadeTo(gameObject, iTween.Hash("name", "fadetox", "alpha", 0, "looptype", "pingpong", "time", 0.18, "oncomplete", "FadeCount", "oncompletetarget", this.gameObject, "oncompleteparams", "x"));
        }
        if (py <= 22 && completey || py >= 899 && completey)
        {
            completey = false;
            iTween.ScaleTo(gameObject, iTween.Hash("name", "compy", "scale", new Vector3(5,3,0), "time", 0.2, "easetype", "linear", "oncomplete", "Uncomp", "oncompletetarget", this.gameObject, "oncompleteparams", "y"));
            iTween.FadeTo(gameObject, iTween.Hash("name", "fadetoy", "alpha", 0, "looptype", "pingpong", "time", 0.18, "oncomplete", "FadeCount", "oncompletetarget", this.gameObject, "oncompleteparams", "y"));
        }
    }

    void FadeCount(string type) //Is there a better way than having to use this function?
    {
        if(type.Equals("x"))
        {
            if(counterx == 1)
            {
                counterx = 0;
                iTween.StopByName("fadetox");
            } else
            {
                counterx++;
            }
        }
        if (type.Equals("y"))
        {
            if (countery == 1)
            {
                countery = 0;
                iTween.StopByName("fadetoy");
            }
            else
            {
                countery++;
            }
        }
    }
}
