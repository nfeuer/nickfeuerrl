﻿using UnityEngine;
using System.Collections;

public class MouseClick : MonoBehaviour {

    public static bool action = false;
    public static bool turn = false; //false for ally turn
    public static GameObject attacker;
    private MouseClick team;
    private bool selected = false;
    private Vector3 resting;
    private Vector3 levitate;
    public bool foe;
    public GameObject bord;
    public SpriteRenderer rendered;
    private GameObject card;
    public bool ranged = false;
    public Vector3 mag;
    private Vector3 adjust = new Vector3(-0.045f, 0.032f, 0);
    public Color toColorT;
    public Color fromColorT;
    public Color toColorF;
    public Color fromColorF;
    // 1 action per card; 3 per turn;

    void Start()
    {
        card = gameObject;
        resting = card.transform.position;
        levitate = new Vector3(0, 0, 0); //0 for now
        if(transform.position.y > 0)
        {
            foe = true;
        } else
        {
            foe = false;
        }

        //if(foe == false) ## used for testing
        //{
            bord = new GameObject();
            rendered = bord.AddComponent<SpriteRenderer>();
            bord.name = card.name + " Bord";
            rendered.sprite = Resources.Load("Sprite/Square", typeof(Sprite)) as Sprite;
            rendered = bord.GetComponent<SpriteRenderer>();
            rendered.color = new Color(0, 199, 238, 0);
            bord.transform.localScale = new Vector3(0.7644346f, 1.021202f, 1);
            bord.transform.position = card.transform.position + adjust;
            toColorT = new Color(0, 199, 238, 1);
            fromColorT = rendered.color;
            toColorF = new Color(255, 0, 0, 1);
            fromColorF = new Color(255, 0, 0, 0);
        //}
    }

    void Attack(Vector3 target)
    {
        if(ranged)
        {

        } else
        {
            iTween.ValueTo(card, iTween.Hash("from", resting, "to", target, "time", 2f, "delay", 0.3f, "easetype", "easeInElastic", "onupdate", "MoveIt", "oncomplete", "Rebound", "oncompleteparams", "atta"));
            iTween.ValueTo(card, iTween.Hash("from", rendered.color, "to", fromColorT, "time", 0.3f, "onupdate", "Visibility"));
            iTween.ScaleTo(card, iTween.Hash("x", 1, "y", 1, "time", 2f, "delay", 0.3f, "easetype", "easeInElastic"));
        }
        
    }

// =========================================================================================
//                             Mouse Events
// =========================================================================================

    void OnMouseDown ()
    {
        Debug.Log(card.name);
        if(selected == action)
        {
            if (foe == turn)
            {
                //if (selected == action)
                //{
                    if (!selected)
                    {
                        selected = true;
                        action = true;
                        attacker = card;
                        gameObject.transform.position = resting + levitate;
                        iTween.ScaleTo(card, iTween.Hash("x", 1.5, "y", 1.5, "time", 0.5F));
                        card.GetComponent<SpriteRenderer>().sortingOrder = 100;
                        iTween.ScaleTo(bord, iTween.Hash("x", 1.2644346, "y", 1.521202, "time", 0.5F));
                        rendered.color = toColorT;
                    }
                    else
                    {
                        selected = false;
                        action = false;
                        card.transform.position = resting;
                        iTween.ScaleTo(card, iTween.Hash("x", 1, "y", 1, "time", 0.5F));
                        iTween.ScaleTo(bord, iTween.Hash("x", 0.7644346, "y", 1.021202, "time", 0.5F));
                        Debug.Log("Deselected");
                    }
                //}
            }
        } else
        {
           team = attacker.GetComponent < MouseClick > ();
           if (foe == team.foe)
           {
                selected = true;
                action = true;
                card.transform.position = resting + levitate;
                iTween.ScaleTo(card, iTween.Hash("x", 1.5, "y", 1.5, "time", 0.5F));
                iTween.ScaleTo(bord, iTween.Hash("x", 1.2644346, "y", 1.521202, "time", 0.5F));
                rendered.color = toColorT;
                team.selected = false;
                team.transform.position = team.resting;
                iTween.ScaleTo(attacker, iTween.Hash("x", 1, "y", 1, "time", 0.5F));
                iTween.ScaleTo(team.bord, iTween.Hash("x", 0.7644346, "y", 1.021202, "time", 0.5F));
                attacker = card;
            } else
            {
                mag = new Vector3(0.5f, 0.5f, 0);
                team.Attack(resting);
                iTween.ShakePosition(card, iTween.Hash("amount", mag, "time", 0.2f, "delay", 2.3f));
            }
            
        }    
    }

    void OnMouseEnter()
    {
        
        if(!selected)
        {
            if (turn != foe)
            {
                rendered.color = fromColorF;
                iTween.ValueTo(card, iTween.Hash("from", rendered.color, "to", toColorF, "time", 0.3f, "onupdate", "Visibility"));
                Debug.Log("enter");
            } else
            {
                rendered.color = fromColorT;
                iTween.ValueTo(card, iTween.Hash("from", rendered.color, "to", toColorT, "time", 0.3f, "onupdate", "Visibility"));
                Debug.Log("enter");
            }
        }     
    }

    void OnMouseExit()
    {
        if (!selected)
        {
            if (turn != foe)
            {
                iTween.ValueTo(card, iTween.Hash("from", rendered.color, "to", fromColorF, "time", 0.3f, "onupdate", "Visibility"));
                Debug.Log("exit");
            }
            else
            {
                iTween.ValueTo(card, iTween.Hash("from", rendered.color, "to", fromColorT, "time", 0.3f, "onupdate", "Visibility"));
                Debug.Log("exit");
            }          
        }          
    }

// =========================================================================================
//                                  iTween Callback Functions
// =========================================================================================


    void Rebound(string typ)
    {
        if(typ.Equals("atta"))
        {
            iTween.ValueTo(card, iTween.Hash("from", card.transform.position, "to", resting, "time", 1f, "onupdate", "MoveIt", "oncomplete", "Change"));
            iTween.ScaleTo(card, iTween.Hash("x", 1.5, "y", 1.5, "time", 1f));
        }
    }

    void Visibility(Color color)
    {
        Debug.Log("YESSSSSSSSSSS");
        rendered.color = color;
    }

    void MoveIt(Vector3 npos)
    {
        card.transform.position = npos;
    }

    void Change()
    {
        iTween.ScaleTo(card, iTween.Hash("x", 1, "y", 1, "time", 0.5F));
        iTween.ScaleTo(bord, iTween.Hash("x", 0.7644346, "y", 1.021202, "time", 0.5F));
        card.GetComponent<SpriteRenderer>().sortingOrder = 0;
        selected = false;
        action = false;
        turn = !turn;
    }

    void StopIt(string nam)
    {
        iTween.StopByName(nam);
    }


    }
