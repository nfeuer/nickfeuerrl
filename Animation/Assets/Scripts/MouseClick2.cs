﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class MouseClick2 : MonoBehaviour {

    //Card render variables
    GameObject card;
    SpriteRenderer rendC;
    GameObject bord;
    SpriteRenderer rendB;
    float sizeW;
    float sizeH;
    float pad;
    Color dormant = new Color(0, 199, 238, 0);
    Color highlight = new Color(0, 199, 238, 1);
    Color dormantF = new Color(255, 0, 0, 0);
    Color highlightF = new Color(255, 0, 0, 1);
    float raiseW;
    float raiseH;
    private Vector3 screenPoint;
    private Vector3 offset;

    //Game variables
    public static bool action = false;
    public static bool turn = false; //false for ally turn
    public static int actionCounter = 0;
    float center;

    //Interaction variables
    public static GameObject attacker;
    private MouseClick2 team;
    private bool selected = false;
    public bool foe;
    public Vector3 cardPos;
    public Vector3 mag;
    //EventTrigger trigger;

    void Start()
    {
        
        //Card render
        sizeW = BoardGenerator2.cardW;
        sizeH = BoardGenerator2.cardH;
        raiseH = sizeH * 1.5f;
        raiseW = sizeW * 1.5f;
        pad = (BoardGenerator2.gridH - sizeH) / 2;
        center = BoardGenerator2.height / 2;
        card = gameObject;
        rendC = card.AddComponent<SpriteRenderer>();

        //Status init
        if (transform.position.y > center)
        {
            foe = false;
            rendC.sprite = BoardGenerator2.allCards[26];
        }
        else
        {
            foe = true;
            rendC.sprite = BoardGenerator2.allCards[0];
        }

        
        //rendC.sortingLayerName = "Card";
        rendC.sortingOrder = 1;
        Debug.Log(sizeH + " " + sizeW);
        card.transform.localScale = new Vector3(sizeW, sizeH, 1);
        card.AddComponent<BoxCollider2D>();
        bord = card.transform.GetChild(0).gameObject;
        rendB = bord.AddComponent<SpriteRenderer>();
        bord.name = card.name + " Border";
        rendB.sprite = Resources.Load("Sprite/Border", typeof(Sprite)) as Sprite;
        bord.transform.localScale = new Vector3(0.75f, 1, 1);
        rendB.color = dormant;
        cardPos = card.transform.position;
        Debug.Log(sizeW);

        

    }

    void Attack(Vector3 target)
    {
        actionCounter++;
        iTween.ValueTo(card, iTween.Hash("from", cardPos, "to", target, "time", 2f, "delay", 0.3f, "easetype", "easeInElastic", "onupdate", "MoveIt", "oncomplete", "Rebound", "oncompleteparams", "atta"));
        iTween.ValueTo(card, iTween.Hash("from", rendB.color, "to", dormant, "time", 0.3f, "onupdate", "Visibility"));
        iTween.ScaleTo(card, iTween.Hash("x", sizeW, "y", sizeH, "time", 2f, "delay", 0.3f, "easetype", "easeInElastic"));
        

    }


    void OnMouseDown()
    {
        offset = card.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        Debug.Log(card.name);
        if (selected == action) 
        {
            if (foe == turn) 
            {
                
                if (!selected)
                {
                    selected = true;
                    action = true;
                    attacker = card;
                    rendC.sortingOrder = 2;
                    iTween.ScaleTo(card, iTween.Hash("x", raiseW, "y", raiseH, "time", 0.5F));
                    card.GetComponent<SpriteRenderer>().sortingOrder = 100;
                    //iTween.ScaleTo(bord, iTween.Hash("x", 1.125, "y", 1.5, "time", 0.5F));
                    rendB.color = highlight;
                }
                else
                {
                    selected = false;
                    action = false;
                    rendC.sortingOrder = 1;
                    iTween.ScaleTo(card, iTween.Hash("x", sizeW, "y", sizeH, "time", 0.5F));
                    //iTween.ScaleTo(bord, iTween.Hash("x", 0.75f, "y", 1, "time", 0.5F));
                    Debug.Log("Deselected");
                }
                //}
            }
        }
        else //When card is being attacked
        {
            team = attacker.GetComponent<MouseClick2>();
            if (foe == team.foe) //Check if selected card is an ally or foe
            {
                selected = true;
                action = true;
                rendC.sortingOrder = 1;
                iTween.ScaleTo(card, iTween.Hash("x", raiseW, "y", raiseH, "time", 0.5F));
                //iTween.ScaleTo(bord, iTween.Hash("x", 1.125, "y", 1.5, "time", 0.5F));
                rendB.color = highlight;
                team.selected = false;
                team.transform.position = team.cardPos;
                iTween.ScaleTo(attacker, iTween.Hash("x", sizeW, "y", sizeH, "time", 0.5F));
                team.rendB.color = dormant;
                //iTween.ScaleTo(team.bord, iTween.Hash("x", 0.7644346, "y", 1.021202, "time", 0.5F));
                attacker = card;
            }
            else
            {
                mag = new Vector3(center/5, center/5, 0);
                team.Attack(cardPos);
                iTween.ShakePosition(card, iTween.Hash("amount", mag, "time", 0.2f, "delay", 2.3f));
            }

        }
    }

    void OnMouseDrag()
    {
        if (selected == action)
        {
            if (foe == turn)
            {
                Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
                Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
                card.transform.position = curPosition;
                cardPos = card.transform.position;
            }
        }
    }

    //public void OnEndDrag(PointerEventData data)
    //{
       // Debug.Log("OnEndDrag called.");
    //}

    

    void OnMouseEnter()
    {
        if (!selected)
        {
            if (turn != foe)
            {
                rendB.color = dormantF;
                iTween.ValueTo(card, iTween.Hash("from", rendB.color, "to", highlightF, "time", 0.3f, "onupdate", "Visibility"));
                Debug.Log("enter");
            }
            else
            {
                rendB.color = dormant;
                iTween.ValueTo(card, iTween.Hash("from", rendB.color, "to", highlight, "time", 0.3f, "onupdate", "Visibility"));
                Debug.Log("enter");
            }
        }
    }

    void OnMouseExit()
    {
        if (!selected)
        {
            if (turn != foe)
            {
                iTween.ValueTo(card, iTween.Hash("from", rendB.color, "to", dormantF, "time", 0.3f, "onupdate", "Visibility"));
                Debug.Log("exit");
            }
            else
            {
                iTween.ValueTo(card, iTween.Hash("from", rendB.color, "to", dormant, "time", 0.3f, "onupdate", "Visibility"));
                Debug.Log("exit");
            }
        }
    }


    // =========================================================================================
    //                                  iTween Callback Functions
    // =========================================================================================


    void Rebound(string typ)
    {
        if (typ.Equals("atta"))
        {
            iTween.ValueTo(card, iTween.Hash("from", card.transform.position, "to", cardPos, "time", 1f, "onupdate", "MoveIt", "oncomplete", "Change"));
            iTween.ScaleTo(card, iTween.Hash("x", raiseW, "y", raiseH, "time", 1f));
        }
    }

    void Visibility(Color color)
    {
        //Debug.Log("YESSSSSSSSSSS");
        rendB.color = color;
    }

    void MoveIt(Vector3 npos)
    {
        card.transform.position = npos;
    }

    void Change()
    {
        iTween.ScaleTo(card, iTween.Hash("x", sizeW, "y", sizeH, "time", 0.5F));
        //iTween.ScaleTo(bord, iTween.Hash("x", 0.75, "y", 1, "time", 0.5F));
        rendC.sortingOrder = 0;
        selected = false;
        action = false;
        if(actionCounter == 3)
        {
            turn = !turn;
            actionCounter = 0;
        }
        
    }

    void StopIt(string nam)
    {
        iTween.StopByName(nam);
    }

    


        
    




}
