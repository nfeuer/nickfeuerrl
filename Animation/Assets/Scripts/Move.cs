﻿using UnityEngine;
using System.Collections;
using UnityEditor;


public class Move : MonoBehaviour {

    public int px = 0;
    public double py = 500;
    public double gravity = -0.1;
    public double velocity = 0;

    void Start()
    {
        

    }

    void Update()
    {
        velocity += gravity;
        py += velocity;
        if (py <= 0)
        {
            //EditorUtility.DisplayDialog("Bottom", "This: " + py, "Ok", "");
            py = 5;
            velocity = -velocity;
            iTween.MoveUpdate(gameObject, iTween.Hash("y", py, "orienttopath", true, "axis", "z"));
            
        }
        else if (py >= 909)
        {
            py = 909;
            velocity = -velocity;
            iTween.MoveUpdate(gameObject, iTween.Hash("y", py, "orienttopath", true, "axis", "z"));
        }
        else
        {
            iTween.MoveUpdate(gameObject, iTween.Hash("y", py, "orienttopath", true, "axis", "z"));
        }

    }

   
    
}
