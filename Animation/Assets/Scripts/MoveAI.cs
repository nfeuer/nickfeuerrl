﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class MoveAI : MonoBehaviour {

    public float angle = 20.0F;
    public float speed = 20.0F;
    public float pos = 0.0F;
    public float segLength = 2.0F;
    public Vector3[] path = new Vector3[100];
    public bool notify = false;
    public float dir;


	// Use this for initialization
	void Start () {
        path[path.Length - 2] = new Vector3(200,200,0);
        path[path.Length - 1] = new Vector3(205,205,0);
        RecalcPath();
	}
	
	// Update is called once per frame
	void Update () {
        iTween.PutOnPath(gameObject, path, pos);
        //Vector3 v3 = iTween.PointOnPath(path, Mathf.Clamp01(pos + 0.01F));
        //Vector3 oldv = iTween.PointOnPath(path, pos);
        //dir = Vector3.Angle(oldv, v3) * Mathf.Rad2Deg;
        //transform.rotation = Quaternion.AngleAxis(dir, Vector3.forward);
        
        pos = pos + Time.deltaTime * speed / path.Length;
        if (pos >= 1.0)
        {
            pos -= 1.0F;
            RecalcPath();
        }
    }

    void RecalcPath()
    {
        Vector3 v3 = path[path.Length - 1] - path[path.Length - 2];
        path[0] = path[path.Length - 1];
        
        for (int i = 1; i < path.Length; i++)
        {
            Quaternion q = Quaternion.AngleAxis(Random.Range(-angle, angle), Vector3.forward);
            v3 = q * v3;
            path[i] = path[i - 1] + v3;
            if(path[i].x <= 0 || path[i].x >= 1065)
            {
                path[i].x = path[i].x - 2 * v3.x;
            }
            if (path[i].y <= 0 || path[i].y >= 909)
            {
                path[i].y = path[i].y - 2 * v3.y;
            }
        }
        //OnDrawGizmos();

        notify = true;

    }

    void OnDrawGizmos()
    {
        iTween.DrawPath(path, new Color(1,1,1,1));
    }


}
