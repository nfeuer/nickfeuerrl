﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class MoveAI2 : MoveAI
{

    
    protected float here = 0.0F;
  

    public Vector3[] tile = new Vector3[100];
    int counter = 0;


    // Use this for initialization
    void Start()
    {
        for(int i = 0; i < tile.Length; i++)
        {
            tile[i] = Vector3.zero;
        }
    }

    // Update is called once per frame
    void Update()
    {
   
            iTween.PutOnPath(gameObject, tile, here);
            Vector3 v3 = iTween.PointOnPath(tile, Mathf.Clamp01(here + 0.01F));
            transform.LookAt(v3, Vector3.up);
            here = here + Time.deltaTime * speed / tile.Length;
            if (here >= 1.0)
            {
                here -= 1.0F;

            }
            if(counter < 4)
        {
            //EditorUtility.DisplayDialog("Bottom", "This: " + Way(), "Ok", "");
        }
        counter++;
    }

    
}
