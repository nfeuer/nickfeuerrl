﻿using UnityEngine;
using System.Collections;

public class Selector : MonoBehaviour
{

    public SpriteRenderer targetRend;
    public Color fromColor;
    public Color toColor;

    // Use this for initialization
    void Start()
    {
        targetRend = gameObject.GetComponent<SpriteRenderer>();
        fromColor = targetRend.color;
        Debug.Log(fromColor);
        toColor = new Color(0, 199, 238, 1);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseEnter()
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", fromColor, "to", toColor, "time", 1f, "onupdate", "Visibility"));
        Debug.Log("enter");
    }

    void OnMouseExit()
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", toColor, "to", fromColor, "time", 1f, "onupdate", "Visibility"));
        Debug.Log("exit");
    }

    void Visibility(Color color)
    {
        Debug.Log("YESSSSSSSSSSS");
        targetRend.color = color;
    }

}
