﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;


public class Disappear : MonoBehaviour {

    public Text message;
    public Button yes;
    public Button no;
    public bool again;
    public GameObject modalPanelObject;
    public GameObject baitButton;

    private static Disappear modalPanel;

    public static Disappear Instance()
    {
        if (!modalPanel)
        {
            modalPanel = FindObjectOfType(typeof(Disappear)) as Disappear;
            if (!modalPanel)
                Debug.LogError("Missing scripts");
        }
        return modalPanel;
    }

    public void Choice (string message, UnityAction yesEvent, UnityAction noEvent)
    {
        modalPanelObject.SetActive(true);
        baitButton.SetActive(false);

        yes.onClick.RemoveAllListeners();
        yes.onClick.AddListener(yesEvent);
        yes.onClick.AddListener(Action);

        no.onClick.RemoveAllListeners();
        no.onClick.AddListener(noEvent);
        no.onClick.AddListener(Action);

        this.message.text = message;
    }

    void Action ()
    {
        modalPanelObject.SetActive(false);
        if(again == true)
        {
            baitButton.SetActive(true);
        }
        
    }

  
}
