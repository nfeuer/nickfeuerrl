﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;

public class GoBack : MonoBehaviour {

    public Button forgiven;

    void Awake()
    {
        forgiven.onClick.RemoveAllListeners();
        forgiven.onClick.AddListener(Yay);
    }

    public void Yay() {

        SceneManager.LoadScene("Modal");

    }
}
