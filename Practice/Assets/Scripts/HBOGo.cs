﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class HBOGo : MonoBehaviour {

    private Disappear modalPanel;

    private UnityAction yesAction;
    private UnityAction noAction;

    void Awake()
    {
        modalPanel = Disappear.Instance();

        yesAction = new UnityAction(YesFunction);
        noAction = new UnityAction(NoFunction);
    }

    
    public void Clicked()
    {
        modalPanel.Choice("Would you like to continue?", YesFunction, NoFunction);
        
    }

    
    void YesFunction()
    {
        modalPanel.again = true;
    }

    void NoFunction()
    {
        modalPanel.again = false;
        SceneManager.LoadScene("Death");
    }

  
}
